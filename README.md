# Wardrobify

Team:

* Person 1 - Matthew Sun
* Person 2 - Brady Billeisen/SEIR ?????

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

First step was to install the Hats app.
I started by making the LocationVO and Hats model going off the required information provided by the instructions.
Then, I estbalished a polling connection between my microservice's LocationVO models and the wardrobe's Location models.
I then created all the views and registered them under the correct URLs before made several test cases for later.
I started on the React components to make a hats list that displayed ALL hats.
Then, I created a form to make a new hat.
I registered the components under the correct routes and links on the Apps/Nav files.
Finally, I made a delete button for each hat object in the attendee list that will re-render the component once a delete event has happened.
