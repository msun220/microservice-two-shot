import React, { useState, useEffect } from 'react';

function HatForm () {

    const [locations, setLocations] = useState([])
    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const [styleName, setStyleName] = useState("")
    const handleStyleNameChange = (event) => {
        setStyleName(event.target.value)
    }
    const [fabric, setFabric] = useState("")
    const handleFabricChange = (event) => {
        setFabric(event.target.value)
    }
    const [color, setColor] = useState("")
    const handleColorChange = (event) => {
        setColor(event.target.value)
    }
    const [location, setLocation] = useState("")
    const handleLocationChange = (event) => {
        setLocation(event.target.value)
    }
    const [pictureUrl, setPictureUrl] = useState("")
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location

        const HatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(HatUrl, fetchConfig);
        if (response.ok) {
            setStyleName("")
            setFabric("")
            setColor("")
            setLocation("")
            setPictureUrl("")
        }

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Treat yourself to a new hat!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={styleName} onChange={handleStyleNameChange} placeholder="Style" required
                        type="text" name="styleName" id="styleName"
                        className="form-control" />
                <label htmlFor="styleName">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required
                        type="text" name="fabric" id="fabric"
                        className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required
                        type="text" name="color" id="color"
                        className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose your hat's location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.closet_name}
                    </option>
                    );
                })}
              </select>
              </div>
              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture URL"
                        type="text" name="pictureUrl" id="pictureUrl"
                        className="form-control" />
                <label htmlFor="color">Picture URL</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default HatForm
