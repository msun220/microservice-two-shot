import React, { useState, useEffect } from 'react';

function HatList() {

    const [hats, setHats] = useState([])
    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const deleteHat = async (value) => {
        const delUrl = "http://localhost:8090"+value
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(delUrl, fetchConfig)
        if (response.ok) {
            fetchData();
        }
    }


    const pictureUrl = (hat) => {
        if (hat.picture_url === null || hat.picture_url === "") {
            return "This hat has no picture :("
        } else {
            return <a href={hat.picture_url}>Image here</a>
        }
    }


    return (

        <table className="table table-striped mt-3">
            <thead>
                <tr>
                <th>Style</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
                <th>Image</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                return (
                    <tr key={hat.href}>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location }</td>
                    <td>{ pictureUrl(hat) }</td>
                    <td><button onClick={() => deleteHat(hat.href)}> Delete </button></td>
                    </tr>
                );
                })}
            </tbody>
        </table>
    )
}

export default HatList
