function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <img class="bg-white rounded shadow d-block mx-auto mb-4"
              src="https://media.gq.com/photos/563a788bdf121979786a276d/master/w_1600,c_limit/SID-DC-11.jpg"
              width="600"/>
      </div>
    </div>
  );
}

export default MainPage;
